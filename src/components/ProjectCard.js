import React from 'react';
import { Cell } from 'react-mdl'
import github from '../assets/icons/github.svg';


const ProjectCard = ({project : { name, image, deployed_url, github_url }}) => {
    return(
        <Cell col={4} className="projectCard">
            <figure className="projectCard_wrapper">
                <a href={deployed_url} target="_blank">
                    <img src={image} alt={name} className="projectCard_image"/>
                </a>
                <div className="prjectCard_title">
                    <a href={github_url} target="_black">
                        <img src={github} alt="github link" className="projectCard_icon"/>
                    </a>
                    {name}
                </div>
            </figure>
        </Cell>
    )
}


export default ProjectCard;