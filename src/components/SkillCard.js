import React from "react";
import { Cell } from 'react-mdl';



const Skillcard = ({ skill }) => (
  <Cell col={6}>
    <div className="skill-card">
      <img src={skill.icon} alt="icon" className="skill-card_icon" />
      <div className="skill-card-body">
        <h6 className="skill-card_title">{skill.title}</h6>
        <p className="skill-card_content">{skill.about}</p>
      </div>
    </div>
  </Cell>
);

export default Skillcard;
