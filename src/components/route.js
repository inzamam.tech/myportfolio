import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./home";
import Resume from "./resume";
import AboutMe from "./about";
import Projects from "./projects";
import Contact from "./contact";
import Resume1 from "./resume1";
import CV from './cv';
import Projects1 from './projects1';
import About1 from './about1';
import { Redirect } from "react-router-dom/cjs/react-router-dom.min";

const Main = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/resume" component={Resume} />
    <Route path="/aboutme" component={AboutMe} />
    <Route path="/projects" component={Projects} />
    <Route path="/contact" component={Contact} />
    <Route path="/resume1" component={Resume1} />
    <Route>
        <Redirect to="/"/>
    </Route>
  </Switch>
);

export const Submain = () => (
  <Switch>
    <Route path="/resume1/about1" component={About1} />
    <Route path="/resume1/cv" component={CV} />
    <Route path="/resume1/projects1" component={Projects1} />
  </Switch>
);

export default Main;
