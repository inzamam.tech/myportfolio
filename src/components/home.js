import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

class Home extends Component {


    render () {
        return (
            <div style={{width:'100%', margin:'auto'}}>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <img
                            src="https://www.kindpng.com/picc/m/22-223941_transparent-avatar-png-male-avatar-icon-transparent-png.png"
                            alt="avatar"
                            className="avatar-img"
                        />
                       <div className="banner-text">
                           <h1>Full Stack Web Developer</h1>
                           <hr/>
                           <p>HTML/CSS | Bootstrap | Javascript | React | React Native | NodeJS | Express | MongoDB</p>
                           <div className="social-link">
                               <a href="http://google.com" rel="noopener noreferrer" target="_black">
                                    <i class="fa fa-linkedin-square"  aria-hidden="true"/>
                               </a>
                               <a href="http://google.com" rel="noopener noreferrer" target="_black">
                                    <i class="fa fa-github-square"  aria-hidden="true"/>
                               </a>
                               <a href="http://google.com" rel="noopener noreferrer" target="_black">
                                    <i class="fa fa-free-code-camp"  aria-hidden="true"/>
                               </a>
                               <a href="http://google.com" rel="noopener noreferrer" target="_black">
                                    <i class="fa fa-youtube-square"  aria-hidden="true"/>
                               </a>
                           </div>
                       </div> 
                    </Cell>
                </Grid>
            </div>
        )
    }
}


export default Home;