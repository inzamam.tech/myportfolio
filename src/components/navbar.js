import React, { useState, useEffect } from 'react';
import { Grid, Cell } from 'react-mdl';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';

const Navbar = () => {

    const [active, setActive] = useState('About');

    useEffect(() => {
        let currentUrl = window.location.href;
        if(currentUrl.endsWith('/'))
            setActive('About')
        else if(currentUrl.endsWith('/projects'))
            setActive('Projects')
        else if(currentUrl.endsWith('/resume'))
            setActive('Resume')
    }, [active])

    const navbar_variant = {
        hidden: {
            y: '-30vh',
            opacity: 0
        },
        visible: {
            y:0,
            opacity: 1
        }
    }

    return (
        <motion.div className="navbar"
            variants={navbar_variant}
            initial='hidden'
            animate='visible'
        
        >
            <div col={8} className="navbar_active">
                <div>{active}</div>
            </div>
            <div col={4} className="navbar_items">
                {active !== 'About' && 
                   <Link to="/resume1/about1">
                        <div className="navbar_item" onClick={()=>setActive('About')} style={{marginRight:10, marginLeft:10}}>About</div>
                   </Link> 
                }
                {
                    active !== 'Resume' ?
                    <Link to="/resume1/cv">
                        <div className="navbar_item" onClick={()=>setActive('Resume')} style={{marginRight:10, marginLeft:10}}>Resume</div>
                        
                    </Link> : null
                }
                {
                    active !== "Projects" && 
                    <Link to="/resume1/projects1">
                        <div className="navbar_item" onClick={()=>setActive('Projects')} style={{marginRight:10, marginLeft:10}}>Projects</div>
                    </Link>

                }
            </div>
        </motion.div>
    )
}

export default Navbar