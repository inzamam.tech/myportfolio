import React, { Component } from "react";
import { Grid, Cell } from "react-mdl";
import react from '../assets/icons/react.svg';
import Bar from './Bar';

const languages  = [
    {
        icon:react,
        name: "Python",
        level:'65'
    },
    {
        icon:react,
        name: "Python",
        level:'45'
    },
    {
        icon:react,
        name: "Python",
        level:'40'
    }
];

const tools = [
    {
        icon:react,
        name: "Python",
        level:'50'
    },
    {
        icon:react,
        name: "Python",
        level:'25'
    },
    {
        icon:react,
        name: "Python",
        level:'60'
    }
]


class CV extends Component {
  render() {
    return (
      <Grid className="resume">
        <Cell col={6} className="resume-card">
          <h4 className="resume-card_heading">Education</h4>
          <div className="resume-card_body">
            <h5 className="resume-card_title">Computer Science Engineering</h5>
            <p className="resume-card_name">Academy of Technology</p>
            <p className="resume-card_details">
              adanndaasndj jhashd sahd adsjads njadsn asdasjdh asdasdb adsjads
              jads jhsad asdasjdhasdjasd asdnasdn
            </p>
          </div>
        </Cell>
        <Cell col={6} className="resume-card">
          <h4 className="resume-card_heading">Education</h4>
          <div className="resume-card_body">
            <h5 className="resume-card_title">Computer Science Engineering</h5>
            <p className="resume-card_name">Academy of Technology</p>
            <p className="resume-card_details">
              adanndaasndj jhashd sahd adsjads njadsn asdasjdh asdasdb adsjads
            </p>
          </div>
        </Cell>
        <Cell col={6}>
            <div className="resume-language">
                <h5 className="resume-language_heading">
                    Language and Framework
                </h5>
                <div className="resume-language_body">
                    {
                        languages.map(language => 
                            <Bar value={language}/>)
                    }
                </div>
            </div>
        </Cell>
        <Cell col={6}>
            <div className="resume-language">
                <h5 className="resume-language_heading">
                    Tools and Software
                </h5>
                <div className="resume-language_body">
                    {
                        tools.map(tool => 
                            <Bar value={tool}/>)
                    }
                </div>
            </div>
        </Cell>
      </Grid>
    );
  }
}

export default CV;
