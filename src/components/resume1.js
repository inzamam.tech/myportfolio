import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import Sidebar from './sidebar';
import Navbar from './navbar';
import { Submain }  from './route'
class Resume1 extends Component {


    render(){
        return(
            <Grid className="resume-body">
                <Cell className="app_sidebar" col={3}>
                    <Sidebar />
                </Cell>
                <Cell className="app_main-content" col={9}>
                    <Navbar />
                    <Submain/>
                </Cell>
            </Grid>
        )
    }
}


export default Resume1;