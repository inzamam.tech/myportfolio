import React, { Component, useState } from "react";
import { Grid, Cell } from 'react-mdl';
import data_projects from './data/projects_data';
import ProjectCard from './ProjectCard';


const Projects1 = () => {

        const [projects, setProjects ] =  useState(data_projects);

        const handleFilterCtegory = (name) => {
            
            const new_array = data_projects.filter(project => project.category.includes(name))
            setProjects(new_array)
        }   


        return (
            <Grid className="projects">
                    <div col={12} className="projects_navbar">
                        <div onClick={() => setProjects(data_projects)}>All</div>
                        <div onClick={() => handleFilterCtegory('react.js')}>React</div>
                        <div onClick={() => handleFilterCtegory('mongoDB')}>MongoDB</div>
                        <div onClick={() => handleFilterCtegory('node.js')}>Node</div>
                        <div onClick={() => handleFilterCtegory('vanilla')}>Vanilla</div>
                    </div>
                    <Cell col={12} className="project_content">
                        {
                            projects.map(project => 
                                <ProjectCard key={project.name} project={project} />)
                        }
                    </Cell>

            </Grid>
        )
}


export default Projects1;