import React, { Component } from 'react';
import facebook from '../assets/icons/facebook.svg';
import instagram from '../assets/icons/instagram.svg';
import github from '../assets/icons/github.svg';
import pin from '../assets/icons/pin.svg';
import tie from '../assets/icons/tie.svg';
import mightycoder from '../assets/mightycoder.svg';
import resume from '../assets/resume.pdf';
import { motion } from 'framer-motion';

const Sidebar = () => {

    const handleEmailMe = () => {
        window.open("mailto:inzamam6161@gmail.com")
    }

    const sidebar_variant = {
        hidden: {
            x: '-10vh',
            opacity:0
        },
        visible: {
            x: 0,
            opacity:1
        }
    }

    return (
       <motion.div
            variants={sidebar_variant}
            initial='hidden'
            animate='visible'
       >
           <img src={mightycoder} alt="avatar" className="sidebar_avatar"/>
           <div className="sidebar_name">Inzamamul <span>Haque</span></div>
           <div className="sidebar_item sidebar_title">Web Developer</div>
           <a href={resume} download="resume.pdf">
               <div className="sidebar_item sidebar_resume">
                    <img src={tie} alt="resume" className="sidebar_icon"/>
                    Download Resume
               </div>
           </a>
           <figure className="sidebar_social-icons">
               <a href=""><img src={facebook} alt="facebook" className="sidebar_icon"/></a>
               <a href=""><img src={instagram} alt="instagram" className="sidebar_icon"/></a>
           </figure>
           <div className="sidebar_contact">
               <div className="sidebar_item sidebar_github">
                    <a href="">
                        <img src={github} alt="github" className="sidebar_icon" style={{marginRight:'10px'}}/>
                        github
                    </a>
               </div>
               <div className="sidebar_location">
                   <img src={pin} alt="location" className="sidebar_icon"  style={{marginRight:'10px'}}/>
                   India
                </div>
               <div className="sidebar_item">inzamam6161@gmail.com</div>
               <div className="sidebar_item">9591961760</div>
               <div className="sidebar_item sidebar_email" onClick={handleEmailMe}><p>Email me</p></div>
           </div>
       </motion.div>
    )
}


export default Sidebar;