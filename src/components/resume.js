import React, { Component } from "react";
import { Grid, Cell } from "react-mdl";
import Education from "./education";
import Experiences from "./experiences";
import Skills from './skills';


class Resume extends Component {
  render() {
    return (
      <div>
        <Grid>
          <Cell col={4}>
            <div className={{ textAlign: "center" }}>
              <img
                src="https://cdn2.iconfinder.com/data/icons/avatar-2/512/Fred_man-512.png"
                alt="avatar"
                style={{ height: "200px" }}
              />
              <h2 style={{ paddingTop: "2em" }}>Inzamamul Haque</h2>
              <h4 style={{ color: "grey" }}>Programmer</h4>
              <hr style={{ borderTop: "3px solid #833fb2", width: "50%" }} />
              <p>
                asdjksakdsajkd hhjadbsa jksad asb jaskdh khasdbmmnadsbm jbasdgha
                jnsakjd ioasjd iasdh ljiads nliasdb lhasd jjhadsh
                sadjbkasdhsagdjsa jahsd
              </p>
              <hr style={{ borderTop: "3px solid #833fb2", width: "50%" }} />
              <p>1 hacker way menlo par, 94025</p>
              <h5>Phone</h5>
              <p>(123) 456-789</p>
              <h5>Email</h5>
              <p>inzamam6161@gmail.com</p>
              <h5>Web</h5>
              <p>mywebsite.com</p>
              <hr style={{ borderTop: "3px solid #833fb2", width: "50%" }} />
            </div>
          </Cell>
          <Cell className="resume-right-col" col={8}>
            <h2>Education</h2>
            <Education
              startYear={2002}
              endYear={2016}
              schoolName="My Unversity"
              schoolDescription="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries."
            />
            <Education
              startYear={2002}
              endYear={2016}
              schoolName="My Unversity"
              schoolDescription="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries."
            />
            <hr style={{ borderTop: "3px solid #e22947" }} />
            <h2>Experiences</h2>
            <Experiences
              startYear={2009}
              endYear={2010}
              jobName="MY company"
              jobDescription="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries."
            />

            <Experiences
              startYear={2009}
              endYear={2010}
              jobName="MY company"
              jobDescription="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries."
            />
            <hr style={{ borderTop: "3px solid #e22947" }} />
            <h2>Skills</h2>
            <Skills skill="Javascript" progress={100}/>
            <Skills skill="Javascript" progress={75}/>
            <Skills skill="Javascript" progress={50}/>
            <Skills skill="Javascript" progress={25}/>

          </Cell>
        </Grid>
      </div>
    );
  }
}

export default Resume;
